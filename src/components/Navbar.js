import React from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";

class Navbar extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="navbar">
        <div className="name">Digital Green</div>
        <div className="link-div">
          <Link to="/" className="links">
            Home
          </Link>
          <Link to="/about" className="links">
            About Us
          </Link>
          <Link to="/input" className="links">
            Sign Up
          </Link>
        </div>
      </div>
    );
  }
}

export default Navbar;
