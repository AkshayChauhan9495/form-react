import React from "react";
import { Link } from "react-router-dom";
import "./Footer.css";

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <footer>
          <div className="content">
            <div className="left box">
              <div className="upper">
                <div className="topic">About us</div>
                <p>
                  Digital Green is a global development organization that
                  empowers smallholder farmers to lift themselves out of poverty
                  by harnessing the collective power of technology and
                  grassroots-level partnerships.
                </p>
              </div>
              <div className="lower">
                <div className="topic">Contact us</div>
                <div className="phone">
                  <a href="#">+91 956036****</a>
                </div>
                <div className="email">
                  <a href="#">abc@gmail.com</a>
                </div>
              </div>
            </div>
            <div className="middle box">
              <div className="topic">Our Work</div>
              <div>
                <a href="#">Access & Diagnose Systems</a>
              </div>
              <div>
                <a href="#">Integrate technology</a>
              </div>
              <div>
                <a href="#">Build Community</a>
              </div>
            </div>
            <div className="right box">
              <div className="topic">Subscribe us</div>
            </div>
          </div>
          <div className="bottom">
            <p>
              Copyright © 2021 <a href="#">Digital Green</a> All rights reserved
            </p>
          </div>
        </footer>
      </>
    );
  }
}

export default Footer;
