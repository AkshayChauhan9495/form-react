import React from "react";
import "./input.css";
import validator from "validator";
// import isAlpha from "validator/lib/isAlpha";
class InputFirst extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      age: "",
      password: "",
      confirmPassword: "",
      agreement: false,
      error: {},
    };
  }

  onChange = (e) => {
    if (this.state.successMessage) {
      this.setState({ successMessage: "" });
    }
    console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.value });
  };
  handleChange = (e) => {
    if (this.state.agreement) {
      this.setState({
        errorAgreement: "",
      });
    }
    this.setState({ agreement: e.target.checked });
  };

  submitForm() {
    let boolVar = true;
    const { username, email, age, password, confirmPassword, agreement } =
      this.state;
    const errors = {};
    if (
      !validator.isLength(username, { min: 3, max: undefined }) ||
      !validator.isAlpha(username, "en-US", { ignore: " -" })
    ) {
      errors.errorUsername = "*Name should be more than 2 aplhabets.";
      boolVar = false;
    } else {
      errors.errorUsername = "";
      boolVar = true;
    }
    if (
      !validator.isInt(age, { gt: 17, lt: 100, allow_leading_zeroes: false })
    ) {
      errors.errorAge = "*Age must be more than 17 and not more than 100.";
      boolVar = false;
    } else {
      errors.errorAge = "";
      boolVar = true;
    }
    if (
      !validator.isEmail(email, {
        blacklisted_chars: "#, &,*, !,$, %, ^, &, (,)",
      })
    ) {
      errors.errorEmail = "*Invalid Email";
      boolVar = false;
    } else {
      errors.errorEmail = "";
      boolVar = true;
    }
    if (
      !validator.isStrongPassword(password, {
        minLength: 8,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
      })
    ) {
      errors.errorPassword =
        "*Password contains: 8 char, 1 No., 1 uppercase, 1 lowerCase, 1 symbol ";
      boolVar = false;
    } else {
      errors.errorPassword = "";
    }
    if (!validator.equals(password, confirmPassword)) {
      errors.errorConfirmPassword = "*Password Didn't match";
      boolVar = false;
    } else {
      errors.errorConfirmPassword = "";
    }
    if (agreement === false) {
      errors.errorAgreement = "*Please accept Terms and Conditions";
      boolVar = false;
    } else {
      errors.errorAgreement = false;
    }
    this.setState({
      error: errors,
    });
    return boolVar;
  }

  onSubmit = (e) => {
    e.preventDefault();
    if (this.submitForm(this.state)) {
      this.setState({
        username: "",
        email: "",
        age: "",
        password: "",
        confirmPassword: "",
        agreement: false,
        successMessage: "Form Submitted Successfully",
      });
    }
  };

  render() {
    const { username, email, age, password, confirmPassword } = this.state;

    return (
      <div className="app ">
        <form onSubmit={this.onSubmit} className="input-form">
          <div>{this.state.successMessage}</div>
          <h1 class="header">Sign Up</h1>
          <div>
            <label className="input">
              Name:
              <input
                className="field"
                type="text"
                value={username}
                onChange={this.onChange}
                name="username"
                placeholder="Enter Your Name"
              />
              <p className="errors">{this.state.error.errorUsername}</p>
            </label>
          </div>
          <div>
            <label className="input">
              Email:
              <input
                // className="field "
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                type="text"
                value={email}
                onChange={this.onChange}
                name="email"
                placeholder="Enter Your Email"
              />
              <p className="errors">{this.state.error.errorEmail}</p>
            </label>
          </div>
          <div>
            <label className="input">
              Age:
              <input
                className="field"
                type="number"
                value={age}
                onChange={this.onChange}
                name="age"
                placeholder="Enter Your Age"
              />
              <p className="errors">{this.state.error.errorAge}</p>
            </label>
          </div>
          <div>
            <label className="input">
              Password:
              <input
                className="field"
                type="password"
                placeholder="Enter Your Password"
                value={password}
                onChange={this.onChange}
                name="password"
              />
              <p className="errors">{this.state.error.errorPassword}</p>
            </label>
          </div>
          <div>
            <label className="input">
              Confirm Password:
              <input
                className="field"
                type="password"
                placeholder="Re-Enter Password"
                value={confirmPassword}
                onChange={this.onChange}
                name="confirmPassword"
              />
              <p className="errors">{this.state.error.errorConfirmPassword}</p>
            </label>
          </div>

          <label className="checks">
            <span>
              <input
                type="checkbox"
                checked={this.state.agreement}
                onChange={this.handleChange}
                name="confirmPassword"
                className="check"
              />
              I have read the{" "}
              <span className="terms">terms and conditions</span>.
              <p className="errors">{this.state.error.errorAgreement}</p>
            </span>
            <input className="btn" type="submit" value="Sign Up" />
          </label>
        </form>
      </div>
    );
  }
}

export default InputFirst;
