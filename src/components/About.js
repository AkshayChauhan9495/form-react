import React from "react";
import "./About.css";
import banner from "../assests/about.jpg";

class About extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <div className="container-about">
          <div className="banner-img">
            <img src={banner} className="bg" />
          </div>
          <div className="first-container">
            <div className="topic-1">
              Digital Green is a global development organization that empowers
              smallholder farmers to lift themselves out of poverty by
              harnessing the collective power of technology and grassroots-level
              partnerships.
            </div>
            <div className="topic-2">
              When farmers have the tools they need to connect with one another,
              they’re far more likely to apply what they’ve learned on their
              farms and in their households – improving their own livelihoods
              and those of others in their community, in a manner that’s
              nutrition-sensitive, climate-resilient, and inclusive. Since day
              one, our deeply committed, curious and collaborative team has been
              challenged and inspired. We’ve tried and failed and tried again,
              and have ultimately become leaders in using technology for global
              development.
            </div>
          </div>
          <div className="work">
            <h1>Our Work</h1>
            <p>
              Together with our grassroots partners, we create digital solutions
              for rural communities around the world. How do we know what’s
              appropriate? Simple. We listen closely – to people, and to data.
              Then we build technology that’s of the community and for the
              community.
            </p>
          </div>
        </div>
      </>
    );
  }
}

export default About;
