import React from "react";
import "./Home.css";
import banner from "../assests/pexels.jpg";
class Home extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="container-home">
        <img src={banner} className="bg-img" />
        <heading className="container">
          <h1>Global Impact</h1>
          <p>
            Digital Green was founded on the belief that technology can
            accelerate efforts to end poverty. Since then, our
            technology-enabled approach has reached over 2.3 million of the
            world’s poorest people. And we’re just getting started.
          </p>
          <span>We stand with the world’s smallholder farmers.</span>
          <div>
            Frontline workers who screen the videos in their communities are
            crucial to our success. Since 2012, we have partnered with the
            Government of India’s National Rural Livelihoods Mission (NRLM) and
            its state-level counterparts to train over 12,500 frontline workers
            to use our approach for promotion of sustainable agriculture &
            nutrition practices in over 15,200 villages across India.
          </div>
        </heading>
      </div>
    );
  }
}

export default Home;
